namespace Online_book_shopping.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Online_book_shopping.Models;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.AspNet.Identity;

    internal sealed class Configuration : DbMigrationsConfiguration<Online_book_shopping.Models.BookShoppingDBContext>
    {
        private const string AdminEmail = "SuperAdmin@gmail.com";
        private const string AdminPassword = "Admin_12345";

        private BookShoppingDBContext db = new BookShoppingDBContext();

        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        public static void InitializeIdentityForEF(BookShoppingDBContext db)
        {
            UserManager<ApplicationUser> userManager = 
                new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            RoleManager<IdentityRole> roleManager = 
                new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            // Create the four roles.
            if (!roleManager.RoleExists("Staff"))
            {
                roleManager.Create(new IdentityRole("Staff"));
            }

            if (!roleManager.RoleExists("Customer"))
            {
                roleManager.Create(new IdentityRole("Customer"));
            }
            if (!roleManager.RoleExists("SuperAdmin"))
            {
                roleManager.Create(new IdentityRole("SuperAdmin"));
            }

            // Create the Supper Admin user and make them an admin.
            var superAdmin = new Staff(AdminEmail);
            if (userManager.Create(superAdmin, AdminPassword).Succeeded)
            {
                userManager.SetLockoutEnabled(superAdmin.Id, false);
                userManager.AddToRole(superAdmin.Id, "SuperAdmin");
            }

            // Create the Supper Admin user and make them an admin.
            var staff = new Staff("staff@gmail.com");
            if (userManager.Create(staff, "Staff_12345").Succeeded)
            {
                userManager.SetLockoutEnabled(staff.Id, false);
                userManager.AddToRole(staff.Id, "Staff"); 
            }

            // Create the Supper Admin user and make them an admin.
            //var predefinedCus = db.Users.OfType<Customer>()
            //                    .FirstOrDefault(u => u.UserName == "customer@gmail.com");

            //if (predefinedCus == null)
            //{
            //    var customer = new Customer("customer@gmail.com");
            //    customer.AddCart();
            //    if (userManager.Create(customer, "Customers_12345").Succeeded)
            //    {
            //        userManager.SetLockoutEnabled(customer.Id, false);
            //        userManager.AddToRole(customer.Id, "Customer");
            //    }
            //}

        }

        protected override void Seed(Online_book_shopping.Models.BookShoppingDBContext context)
        {
            InitializeIdentityForEF(context);

            //  This method will be called after migrating to the latest version.

            context.Books.AddOrUpdate(
                            b => b.Title,
                         new Book {
                             Title = "Data Mining: Practical Machine Learning Tools and Techniques, Third Edition (Morgan Kaufmann Series in Data Management Systems) 3rd Edition",
                             Author = "Ian H. Witten, Eibe Frank, Mark A. Hall",
                             Category = "Artificial Intelligence",
                             ImgURL = "https://images-na.ssl-images-amazon.com/images/I/51cOveE-ZQL._SX399_BO1,204,203,200_.jpg",
                             Stock = 10,
                             OverView = "Data Mining: Practical Machine Learning Tools and Techniques offers a thorough grounding in machine learning "
                             + "concepts as well as practical advice on applying machine learning tools and techniques in real-world data mining situations."
                             + "This highly anticipated third edition of the most acclaimed work on data mining and machine learning will teach you everything "
                             + "you need to know about preparing inputs, interpreting outputs, evaluating results, and the algorithmic methods at the heart of "
                             + "successful data mining.Thorough updates reflect the technical changes and modernizations that have taken place in the field since the last edition,"
                             + "including new material on Data Transformations,Ensemble Learning, Massive Data Sets,"
                             + "Multi - instance Learning, plus a new version of the popular Weka machine learning software developed by the authors.Witten,"
                             + "Frank, and Hall include both tried - and - true techniques of today as well as methods at the leading edge of contemporary research.",
                             Price =  39.84,
                             Publisher = "Morgan Kaufmann; 3 edition (January 20, 2011)"
                         },

                          new Book
                          {
                              Title = "The Elements of Statistical Learning: Data Mining, Inference, and Prediction, Second Edition (Springer Series in Statistics) ",
                              Author = "Trevor Hastie , Robert Tibshirani, Jerome Friedman",
                              Category = "Data Mining",
                              ImgURL = "https://images-na.ssl-images-amazon.com/images/I/41aQrQaPseL._SX331_BO1,204,203,200_.jpg",
                              Stock = 15,
                              OverView = "During the past decade there has been an explosion in computation"+
                              " and information technology. With it have come vast amounts of data in a "+
                              "variety of fields such as medicine, biology, finance, and marketing. The "+
                              "challenge of understanding these data has led to the development of new "+
                              "tools in the field of statistics, and spawned new areas such as data mining, "+
                              "machine learning, and bioinformatics. Many of these tools have common "+
                              "underpinnings but are often expressed with different terminology. This book "+
                              "describes the important ideas in these areas in a common conceptual framework. "+
                              "While the approach is statistical, the emphasis is on concepts rather than mathematics. "+
                              "Many examples are given, with a liberal use of color graphics. It is a valuable resource "+
                              "for statisticians and anyone interested in data mining in science or industry. The book's "+
                              "coverage is broad, from supervised learning (prediction) to unsupervised learning. The many "+
                              "topics include neural networks, support vector machines, classification trees and boosting---"+
                              "the first comprehensive treatment of this topic in any book.",
                              Price = 69.16,
                              Publisher = "Morgan Kaufmann; 3 edition (January 20, 2011)"
                          },
                        new Book
                        {
                            Title = "Machine Learning: A Probabilistic Perspective (Adaptive Computation and Machine Learning series) ",
                            Author = "Kevin P. Murphy",
                            Category = "Computer Science",
                            ImgURL = "https://images-na.ssl-images-amazon.com/images/I/51MucLjt9IL._SX439_BO1,204,203,200_.jpg",
                            Stock = 13,
                            OverView = "Today's Web-enabled deluge of electronic data calls for automated "+
                            "methods of data analysis. Machine learning provides these, developing methods"+
                            " that can automatically detect patterns in data and then use the uncovered "+
                            "patterns to predict future data. This textbook offers a comprehensive and "+
                            "self-contained introduction to the field of machine learning, based on a "+
                            "unified, probabilistic approach. The coverage combines breadth and depth, "+
                            "offering necessary background material on such topics as probability, "+
                            "optimization, and linear algebra as well as discussion of recent developments "+
                            "in the field, including conditional random fields, L1 regularization, and deep learning. ",
                            Price = 94.78,
                            Publisher = "The MIT Press; 1 edition (August 24, 2012)"
                        }
            );
        }
    }
}
