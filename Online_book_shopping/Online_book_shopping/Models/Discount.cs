﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Online_book_shopping.Models
{
    public class Discount
    {
        [Key]
        public int DiscountID { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public float DiscountRate { get; set; }

        public DiscountStrategy Strategy { get; set; }

        public virtual List<Book> Books { get; set; }

    }
}