﻿using Online_book_shopping.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace Online_book_shopping.Models
{
    public class DiscountStrategy
    {
        [Key, ForeignKey("Discount")]
        public int DiscountStrategyID { get; set; }

        public virtual Discount Discount { get; set; }

        //public abstract bool IsSatisfied();
    }

    public class RegistrationTimeStrategy : DiscountStrategy
    {
        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public bool IsSatisfied(Customer customer, BookShoppingDBContext db)
        {
            if (DateTime.Compare(EndTime, customer.RegisteTime ) > 0
                && DateTime.Compare(customer.RegisteTime, StartTime) >= 0)
            {
                return true;
            }
            return false;
        }
    }

    public class ShoppingHistoryStrategy : DiscountStrategy
    {
        public float MinTotal { get; set; }

        public float MaxTotal { get; set; }

        public bool IsSatisfied(Customer customer, BookShoppingDBContext db)
        {
            var transactions = db.Transactions
                    .Include(t => t.Payment)
                    .Include(t => t.Recordes.Select(r => r.Book))
                    .Where(t => t.Customer.Id == customer.Id && t.isDeleted == false)
                    .ToList();
            transactions.RemoveAll(t => t.Payment.status != PaymentStatus.Paid);

            double customerTotal = 0;
            foreach (var tran in transactions)
            {
                customerTotal += tran.Total;
            }
            if (MinTotal <= customerTotal && customerTotal < MaxTotal)
            {
                return true;
            }
            return false;
        }
    }

    public class LocationStrategy : DiscountStrategy
    {
        public int MinPostCode { get; set; }

        public int MaxPostCode { get; set; }

        public bool IsSatisfied(Customer customer, BookShoppingDBContext db)
        {
            foreach (var address in customer.Addresses)
            {
                if (MinPostCode <= address.PostCode &&
                    address.PostCode < MaxPostCode)
                {
                    return true;
                }
            }
            return false;
        }
    }
}