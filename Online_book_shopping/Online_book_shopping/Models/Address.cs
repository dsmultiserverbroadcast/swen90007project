﻿using System.ComponentModel.DataAnnotations;

namespace Online_book_shopping.Models
{
    public class Address
    {
        public int AddressID { get; set; }

        public Customer Customer { get; set; }

        public string Country { get; set; }

        public string State { get; set; }

        public string Street1 { get; set; }

        public string Street2 { get; set; }

        [Range(1000, 999999)]
        public long PostCode { get; set; }

        public Address()
        {
            //Country = "AU";
            //State = "VIC";
            //Street1 = "Swanston ST";
            //Street2 = "2813/551";
            //PostCode = 3053;
        }
    }
}