﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online_book_shopping.Models
{
    public class DiscountCreateViewModel
    {
        public int DiscountID { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public float DiscountRate { get; set; }

        public string BookIDs { get; set; }

        public RegistrationTimeStrategy RegistrationTimeStrategy { get; set; }

        public ShoppingHistoryStrategy ShoppingHistoryStrategy { get; set; }

        public LocationStrategy LocationStrategy { get; set; }
    }
}