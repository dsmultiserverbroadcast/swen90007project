﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System;

namespace Online_book_shopping.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public DateTime RegisteTime { get; set; }

        protected ApplicationUser() { }

        public ApplicationUser(string email) : base(email)
        {
            Email = email;
            RegisteTime = DateTime.Now;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class Customer : ApplicationUser
    {
        public Customer() { }

        public Customer(string email) : base(email) { }

        public Cart Cart { get; set; }

        public List<Address> Addresses { get; set; }

        public void AddCart()
        {
            if (Cart == null)
            {
                Cart = new Cart();
            }
        }
    }

    public class Staff : ApplicationUser
    {
        public Staff() { }

        public Staff(string email) : base(email) { }
    }
}