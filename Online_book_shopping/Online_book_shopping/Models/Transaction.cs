﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Online_book_shopping.Models
{
    public enum ShippingStatus { Processing ,Shipped, Arrived, Received, In_Transit}

    public class Transaction
    {
        public int TransactionID { get; set; }

        public virtual List<Record> Recordes { get; set; }

        public virtual Address Address { get; set; }

        public virtual Customer Customer { get; set; }

        //[Required ,ForeignKey("Payment")]
        //public int PaymentID { get; set; }
        public virtual Payment Payment { get; set; }

        public ShippingStatus ShippingStatus { get; set; }

        public bool isDeleted { get; set; }

        public double Total
        {
            get
            {
                double total = 0.0;
                foreach (var record in Recordes)
                {
                    total += record.SubTotal;
                }
                return total;
           }
        }
    }
}