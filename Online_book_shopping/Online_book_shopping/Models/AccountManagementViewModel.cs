﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online_book_shopping.Models
{
    public class AccountManagementViewModel
    {
        public string ID { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Role { get; set; }

        public DateTime RegisterTime { get; set; }

        public AccountManagementViewModel() { }

        public AccountManagementViewModel(string id, string userName, 
            string email, string role, DateTime registerTime)
        {
            ID = id;
            UserName = userName;
            Email = email;
            Role = role;
            RegisterTime = registerTime;
            
        }
    }
}