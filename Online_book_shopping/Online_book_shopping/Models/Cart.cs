﻿using Online_book_shopping.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Online_book_shopping.Models
{
    public class Cart
    {
        [Key]
        public int CartID { get; set; }
        
        public Customer Customer { get; set; }

        public virtual List<Record> Records { get; set; }

        public void AddBook(Customer customer, Book book, int qty , BookShoppingDBContext db)
        {
            foreach (var record in Records)
            {
                if (record.Book == book)
                {
                    record.Quantity += qty;
                    return;
                }
            }

            var rate = new BooksController().GetDiscountRate(customer, book.Discounts, db);

            Records.Add(new Record()
            {
                Book = book,
                Quantity = qty,
                Cart = this,
                CartID = this.CartID,
                DiscountRate = rate
            });

        }
    }
}