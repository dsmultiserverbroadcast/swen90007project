﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Online_book_shopping.Models
{

    public enum PaymentMethods { Null, Debit_Cards, Charge_Cards, Prepaid_Card, Direct_Debit, PayPal }

    public enum PaymentStatus { Unpaid, Paid, Processing, Delayed, Stopped, Not_Approved, Approved }

    public class Payment
    {
        [Key]
        public int PaymentID { get; set; }

        public PaymentMethods PaymentMethod { get; set; }

        public PaymentStatus status { get; set; }

        //[ForeignKey("Transaction")]
        //public int TransactionID { get; set; }
        public virtual Transaction Transaction { get; set; }

    }
}