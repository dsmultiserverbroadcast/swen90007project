﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Online_book_shopping.Models
{
    public class Record
    {
        [Key]
        public int RecordID { get; set; }

        [ForeignKey("Cart")]
        public int? CartID { get; set; }
        public virtual Cart Cart { get; set; }
        
        public Book Book { get; set; }

        public int Quantity { get; set; }

        public Transaction Transaction { get; set; }

        public float DiscountRate { get; set; }

        public double SubTotal { get{ return Math.Round(Book.Price * Quantity * ( 100 - DiscountRate) / 100, 2 ); } }
    }
}