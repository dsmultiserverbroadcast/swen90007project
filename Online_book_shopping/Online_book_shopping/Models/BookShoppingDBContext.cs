﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Online_book_shopping.Models
{
    public class BookShoppingDBContext : IdentityDbContext<ApplicationUser>
    {
            public BookShoppingDBContext()
                : base("DefaultConnection", throwIfV1Schema: false)
            {
            }

            public static BookShoppingDBContext Create()
            {
                return new BookShoppingDBContext();
            }

        public DbSet<Payment> Payments { set; get; }
        public DbSet<Book> Books { set; get; }
        public DbSet<Transaction> Transactions { set; get; }
        public DbSet<Address> Addresses { set; get; }
        public DbSet<Record> Records { set; get; }
        public DbSet<Cart> Carts { set; get; }
        public DbSet<Discount> Discounts { get; set; }
        public DbSet<DiscountStrategy> DiscountStrategies { get; set; }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Customer>()
                .HasRequired(c => c.Cart)
                .WithRequiredDependent(cart => cart.Customer)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Transaction>()
                .HasOptional(t => t.Payment)
                .WithRequired(p => p.Transaction);
            //.WithRequiredDependent(p => p.Transaction)
            //.WillCascadeOnDelete(true);

            //modelBuilder.Entity<Transaction>()
            //    .HasMany<Record>(t => t.Recordes)
            //    .WithOptional()
            //    .WillCascadeOnDelete(true);


            //modelBuilder.Entity<Transaction>()
            //    .HasKey(t => t.TransactionID)
            //    .HasRequired(t => t.Payment)
            //    .WithRequiredDependent(p => p.Transaction);


        }

    }
}