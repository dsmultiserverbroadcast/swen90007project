﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Online_book_shopping.Models
{
    public class Book
    {
        public int BookID { get; set; }

        public string Title { get; set; }

        public double Price { get; set; }

        public string Author { get; set; }

        public string Publisher { get; set; }

        [Range(0, 9999)]
        public int Stock { get; set; }

        public string Category { get; set; }

        public string OverView { get; set; }

        [Url]
        public string ImgURL { get; set; }

        public virtual List<Discount> Discounts { get; set; }
    }
}