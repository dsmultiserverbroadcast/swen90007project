﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Online_book_shopping.Models;

namespace Online_book_shopping.Controllers
{
    [Authorize]
    public class TransactionsController : Controller
    {
        private BookShoppingDBContext db = new BookShoppingDBContext();

        // GET: Transactions
        [Authorize(Roles = "Customer")]
        public ActionResult Index()
        {

            var currentUser = db.Users
                .FirstOrDefault(u => u.UserName == User.Identity.Name);
            var transactions = db.Transactions
                .Include(t => t.Payment)
                .Where(t => t.Customer.Id == currentUser.Id && t.isDeleted == false)
                .ToList();
            var records = db.Records
                .Include(r => r.Book)
                .Include(r => r.Transaction)
                .Where(c => c.Transaction.Customer.Id == currentUser.Id)
                .ToList();
            return View(transactions.ToList());
        }

        // GET: Transactions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Transaction transaction = 
                db.Transactions
                //.Include(t => t.Payment)
                .Include(t => t.Recordes)
                .FirstOrDefault(t => t.TransactionID == id);

            var records = db.Records
                .Include(r => r.Book)
                .Where(c => c.Transaction.TransactionID == transaction.TransactionID)
                .ToList();

            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View(transaction);
        }

        // GET: Transactions/Create
        public ActionResult Create()
        {
            ViewBag.TransactionID = new SelectList(db.Payments, "PaymentID", "PaymentID");
            return View();
        }

        // POST: Transactions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TransactionID,ShippingStatus,Total")] Transaction transaction)
        {
            if (ModelState.IsValid)
            {
                db.Transactions.Add(transaction);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TransactionID = new SelectList(db.Payments, "PaymentID", "PaymentID", transaction.TransactionID);
            return View(transaction);
        }

        // GET: Transactions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            ViewBag.TransactionID = new SelectList(db.Payments, "PaymentID", "PaymentID", transaction.TransactionID);
            return View(transaction);
        }

        // POST: Transactions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TransactionID,ShippingStatus,Total")] Transaction transaction)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transaction).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TransactionID = new SelectList(db.Payments, "PaymentID", "PaymentID", transaction.TransactionID);
            return View(transaction);
        }

        // GET: Transactions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View(transaction);
        }

        // POST: Transactions/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Transaction transaction = db.Transactions
                .Include(t => t.Payment)
                .FirstOrDefault(t => t.TransactionID == id);

            var records = db.Records
                .Include(r => r.Book)
                .Include(r => r.Transaction)
                .Where(c => c.Transaction.TransactionID == transaction.TransactionID)
                .ToList();

            transaction.isDeleted = true;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // POST: Transactions/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Confirmed(Cart cart)
        {
            Console.WriteLine();

            var user = db.Users
                .OfType<Customer>()
                .Include(u => u.Cart)
                .Include( u => u.Addresses)
                .First(u => u.UserName == User.Identity.Name);

            Transaction transaction = new Transaction()
            {
                Address = user.Addresses.First(),
                Customer = user,
                Payment = new Payment()
            };

            transaction.Recordes = db.Records.Include(r => r.Book).Where(r => r.CartID == cart.CartID).ToList();

            foreach (var record in transaction.Recordes)
            {
                user.Cart.Records.Remove(record);
                record.Quantity = cart.Records.FirstOrDefault(r => r.RecordID == record.RecordID).Quantity;
                record.Cart = null;
                record.CartID = null;
                
            }
            db.Payments.Add(transaction.Payment);
            db.Transactions.Add(transaction);
            db.SaveChanges();

            return RedirectToAction("Details/"+ transaction.TransactionID);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PayTransaction(Payment payment)
        {
            var oldPay = db.Payments
                .FirstOrDefault(p => p.PaymentID == payment.PaymentID);

            oldPay.PaymentMethod = payment.PaymentMethod;
            oldPay.status = PaymentStatus.Paid;
            //oldPay.status = PaymentStatus.Processing;

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult WithdrawTransaction(Payment payment)
        //{
        //    var oldPay = db.Payments.FirstOrDefault(p => p.PaymentID == payment.PaymentID);

        //    oldPay.PaymentMethod = payment.PaymentMethod;
        //    oldPay.status = PaymentStatus.Processing;

        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

    }
}
