﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Online_book_shopping.Models;

namespace Online_book_shopping.Controllers
{
    public class DiscountsController : Controller
    {
        private BookShoppingDBContext db = new BookShoppingDBContext();

        // GET: Discounts
        [Authorize(Roles = "Staff")]
        public ActionResult Index()
        {
            var discounts = db.Discounts.Include(d => d.Strategy);
            return View(discounts.ToList());
        }

        [Authorize(Roles = "Staff")]
        // GET: Discounts
        public ActionResult GetStrategyPartial(string type)
        {
            switch (type)
            {
                case "RegistrationTimeStrategy":
                    ViewData.TemplateInfo.HtmlFieldPrefix = "RegistrationTimeStrategy";
                    return PartialView("~/Views/Discounts/RegistrationTimeStraPartial.cshtml", 
                        new RegistrationTimeStrategy());
                case "ShoppingHistoryStrategy":
                    ViewData.TemplateInfo.HtmlFieldPrefix = "ShoppingHistoryStrategy";
                    return PartialView("~/Views/Discounts/ShoppingHistoryStraPartial.cshtml",
                        new ShoppingHistoryStrategy());
                case "LocationStrategy":
                    ViewData.TemplateInfo.HtmlFieldPrefix = "LocationStrategy";
                    return PartialView("~/Views/Discounts/LocationStraPartial.cshtml",
                        new LocationStrategy());
                default:
                    return null;
            }

        }

        // GET: Discounts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discount discount = db.Discounts.Find(id);
            if (discount == null)
            {
                return HttpNotFound();
            }
            return View(discount);
        }

        // GET: Discounts/Create
        [Authorize(Roles = "Staff")]
        public ActionResult Create()
        {
            ViewBag.DiscountID = new SelectList(db.DiscountStrategies, "DiscountStrategyID", "DiscountStrategyID");
            return View();
        }

        // POST: Discounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Staff")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DiscountCreateViewModel discountCreateVM)
        //public ActionResult Create(Discount discount)
        {
            Discount discount = MapFromDiscountCreateVM(discountCreateVM);

            db.Discounts.Add(discount);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public Discount MapFromDiscountCreateVM(DiscountCreateViewModel viewModel)
        {
            Discount discount = new Discount();
            discount.DiscountRate = viewModel.DiscountRate;
            discount.StartTime = viewModel.StartTime;
            discount.EndTime = viewModel.EndTime;
            discount.Books = new List<Book>();

            var chosenBooks = viewModel.BookIDs.Split('*').ToList();
            foreach (var bookid in chosenBooks)
            {
                if (String.IsNullOrEmpty(bookid)){ continue; }
                int intbookid;
                if (int.TryParse(bookid, out intbookid))
                {
                    var bookObj = db.Books.FirstOrDefault(b => b.BookID == intbookid);
                    discount.Books.Add(bookObj);
                }
            }

            if (viewModel.LocationStrategy != null)
            {
                discount.Strategy = viewModel.LocationStrategy;
            }
            else if (viewModel.RegistrationTimeStrategy != null)
            {
                discount.Strategy = viewModel.RegistrationTimeStrategy;
            }
            else if (viewModel.ShoppingHistoryStrategy != null)
            {
                discount.Strategy = viewModel.ShoppingHistoryStrategy;
            }
            return discount;
        }

        // GET: Discounts/Edit/5
        public ActionResult AddingBooks()
        {
            return PartialView("~/Views/Discounts/AllBooksPartial.cshtml",
                db.Books.ToList());
        }

        // GET: Discounts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discount discount = db.Discounts.Find(id);
            if (discount == null)
            {
                return HttpNotFound();
            }
            ViewBag.DiscountID = new SelectList(db.DiscountStrategies, "DiscountStrategyID", "DiscountStrategyID", discount.DiscountID);
            return View(discount);
        }

        // POST: Discounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DiscountID,StartTime,EndTime,DiscountRate")] Discount discount)
        {
            if (ModelState.IsValid)
            {
                db.Entry(discount).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DiscountID = new SelectList(db.DiscountStrategies, "DiscountStrategyID", "DiscountStrategyID", discount.DiscountID);
            return View(discount);
        }

        // GET: Discounts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discount discount = db.Discounts.Find(id);
            if (discount == null)
            {
                return HttpNotFound();
            }
            return View(discount);
        }

        // POST: Discounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Discount discount = db.Discounts.Include(d => d.Strategy)
                .FirstOrDefault(d => d.DiscountID == id) ;
            db.DiscountStrategies.Remove(discount.Strategy);
            db.Discounts.Remove(discount);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
