﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Online_book_shopping.Models;

namespace Online_book_shopping.Controllers
{
    public class CartsController : Controller
    {
        private BookShoppingDBContext db = new BookShoppingDBContext();

        // GET: Carts
        [Authorize(Roles = "Staff")]
        public ActionResult Index()
        {
            return View(db.Carts.ToList());
        }

        [Authorize(Roles = "Customer")]
        // GET: Carts/Details/5
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cart cart = db.Carts.Find(id);
            if (cart == null)
            {
                return HttpNotFound();
            }
            var records = db.Records
                .Include(r => r.Book)
                .Where(r => r.CartID == cart.CartID).ToList();
            return View(cart);
        }

        // GET: Carts/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        // POST: Carts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "CartID")] Cart cart)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Carts.Add(cart);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(cart);
        //}

        // GET: Carts/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Cart cart = db.Carts.Find(id);
        //    if (cart == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(cart);
        //}

        //// POST: Carts/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "CartID")] Cart cart)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(cart).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(cart);
        //}

        //// GET: Carts/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Cart cart = db.Carts.Find(id);
        //    if (cart == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(cart);
        //}

        //// POST: Carts/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Cart cart = db.Carts.Find(id);
        //    db.Carts.Remove(cart);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [Authorize(Roles = "Customer")]
        [HttpPost]
        public ActionResult AddRecord(int BookID, int Quantity = 1)
        {
            var user = db.Users
                .OfType<Customer>()
                .Include(u => u.Cart)
                .Include(u => u.Addresses)
                .Include(u => u.Roles)
                .FirstOrDefault(u => u.UserName == User.Identity.Name);

            var book = db.Books
                .Include(b => b.Discounts.Select(d => d.Strategy))
                .FirstOrDefault(b => b.BookID == BookID);

            var records = db.Records
                .Include(r => r.Book)
                .Where(r => r.CartID == user.Cart.CartID).ToList();

            if (book == null)
            {
                Response.Status = HttpStatusCode.BadRequest.ToString();
            }

            user.Cart.AddBook(user ,book, Quantity, db);

            db.SaveChanges();

            return PartialView("~/Views/Cart/CartPartialView.cshtml", user.Cart);

        }

        [Authorize(Roles = "Customer")]
        [HttpPost]
        public void RemoveRecord(int BookID)
        {
            var user = db.Users
                .OfType<Customer>()
                .Include(u => u.Cart)
                .FirstOrDefault(u => u.UserName == User.Identity.Name);

            var book = db.Books
                .FirstOrDefault(b => b.BookID == BookID);

            var records = db.Records
                .Include(r => r.Book)
                .Where(r => r.CartID == user.Cart.CartID).ToList();

            var tgt = records.FirstOrDefault(r => r.Book.BookID == BookID);

            if (book == null || tgt == null)
            {
                Response.Status = HttpStatusCode.BadRequest.ToString();
            }

            db.Records.Remove(tgt);
            //user.Cart.Records.Remove(tgt);

            db.SaveChanges();

        }
    }
}
