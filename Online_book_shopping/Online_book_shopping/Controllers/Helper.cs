﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online_book_shopping
{
    public static class Helper
    {
        public static string CutTo(string str, int length)
        {

            return str == null 
                    ? str : str.Length > length
                    ? str.Substring(0, length) + "..." : str;
        } 
    }
}