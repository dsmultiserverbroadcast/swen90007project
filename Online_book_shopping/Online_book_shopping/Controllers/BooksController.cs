﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Online_book_shopping.Models;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Online_book_shopping.Controllers
{
    public class BooksController : Controller
    {
        private BookShoppingDBContext db = new BookShoppingDBContext();

        // GET: Books
        public ActionResult Index()
        {
         var bookList = db.Books
                .Include(b => b.Discounts)
                .Include(b => b.Discounts.Select(d => d.Strategy))
                .ToList();
            var user = db.Users
                .Include(u => u.Roles)
                .FirstOrDefault(u => u.UserName == User.Identity.Name);
            
            //To get the best discount
            if (User.IsInRole("Customer"))
            {
                user = db.Users
                    .OfType<Customer>()
                    .Include(u => u.Addresses)
                    .Include(u => u.Roles)
                    .FirstOrDefault(u => u.UserName == User.Identity.Name);
                foreach (var book in bookList)
                {
                    var bestCount = FindBestDiscount((Customer)user, book.Discounts, db);
                    book.Discounts.Clear();
                    if (bestCount != null)
                    {
                        book.Discounts.Add(bestCount); 
                    }
                } 
            }
            else// else the user will not be able to see the discounts
            {
                foreach (var book in bookList)
                {
                    book.Discounts.Clear();
                }
            }

            return View(bookList);
        }

        // GET: Books/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        [Authorize(Roles = "Staff")]
        // GET: Books/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Books/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Staff")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BookID,Title,Price,Author,Publisher,Stock,Category,OverView")] Book book)
        {
            if (ModelState.IsValid)
            {
                db.Books.Add(book);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(book);
        }

        [Authorize(Roles = "Staff")]
        // GET: Books/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: Books/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Staff")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BookID,Title,Price,Author,Publisher,Stock,Category,OverView")] Book book)
        {
            if (ModelState.IsValid)
            {
                db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(book);
        }

        [Authorize(Roles = "Staff")]
        // GET: Books/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: Books/Delete/5
        [Authorize(Roles = "Staff")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Book book = db.Books.Find(id);
            db.Books.Remove(book);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public ActionResult Search(string text)
        {
            var list = db
                .Books
                .Where(
                b => b.Title.Contains(text)
                ).ToList();

            return PartialView("~/Views/Books/BookListPartial.cshtml",list);
        }

        public float GetDiscountRate(Customer customer, List<Discount> discounts, BookShoppingDBContext db)
        {
            var result = FindBestDiscount(customer, discounts, db);
            if (result == null)
            {
                return 0;
            }
            return result.DiscountRate;
        }

        public Discount FindBestDiscount(Customer customer, List<Discount> discounts, BookShoppingDBContext db)
        {
            Discount bestDiscount;
            if (discounts == null || discounts.Count() == 0)
            {
                return null;
            }
            bestDiscount = null;
            foreach (var dis in discounts)
            {
                bool IsSatisfied = false;
                if (dis.Strategy is RegistrationTimeStrategy)
                {
                    IsSatisfied = ((RegistrationTimeStrategy)dis.Strategy)
                        .IsSatisfied(customer, db);
                }
                else if (dis.Strategy is ShoppingHistoryStrategy)
                {
                    IsSatisfied = ((ShoppingHistoryStrategy)dis.Strategy)
                        .IsSatisfied(customer, db);
                }
                else
                {
                    IsSatisfied = ((LocationStrategy)dis.Strategy)
                        .IsSatisfied(customer, db);
                }
                if (IsSatisfied)
                {
                    //will get the best discount
                    if (bestDiscount == null || bestDiscount.DiscountRate > dis.DiscountRate)
                    {
                        bestDiscount = dis;
                    }
                }

            }
            return bestDiscount;
        }


    }
}
