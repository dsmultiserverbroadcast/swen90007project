﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Online_book_shopping.Startup))]
namespace Online_book_shopping
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
